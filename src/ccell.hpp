#include <map>

using namespace std;

/** element of spreadsheet, abstract class with its subtypes of cells which can be printed in output stream */
class CCell
{
  public:
    /** decides which destructor is called, based on cell type */
    virtual        ~CCell     ( void );

    /** 
     * reads content the cell is filled by
     *
     * @return content of the cell
     */
    virtual string GetContent ( void ) const = 0;

    /** 
     * prints cell in output stream
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the cell
     * @param[in,out] os output stream
     */
    virtual void   Print      ( const map < string, CCell * > & cells, const string & name, ostream & os ) const = 0;

    /**
     * evaluates numeric value (detects error for string)
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the original cell, if this name appears in evaluated expression, cycle is detected
     * @param[in,out] invRefInd indicator of reference to string type cell in expression
     * @param[in,out] zeroDivInd indicator of dividing by zero in expression
     * @return numeric value of cell
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in expression
     */
    virtual double Evaluate   ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const = 0;
};