#include "csheet.hpp"

/**
 * @mainpage Spreadsheet
 *
 * simple spreadsheet application, in which you can fill cells with contents of various types, erase them, print them in standard output, save spreadsheet to file and load it again
 */

// interface that reads commands from standard input and calls corresponding CSheet functions, detects errors in commands and their parameters
void Interface ( void );

// assistant function for Interface, gets command from line, calls corresponding CSheet function, detects errors in commands and their parameters
bool Command ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 );

// assistant function for Command, when command isn't Help, gets a parameter, calls corresponding CSheet function, detects errors in commands and their parameters
bool AnotherCommand ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 );

// assistant function for AnotherCommand, when command isn't either Save or Load, calls corresponding CSheet function (gets another parameter when needed), detects errors in commands and their parameters
bool FillPrintErase ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 );

// assistant function for FillPrintErase, when command isn't either Print, Erase or Fill, calls corresponding CSheet function (gets another parameter when needed), detects errors in commands and their parameters
bool Range ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 );

// prints help in standard output
void Help ( void );

void Interface ( void )
{
  CSheet sheet;
  string line;
  string cmd;
  string str1;
  string str2;
  string str3;
  string::const_iterator it;
  cout << "Type help to get help" << endl;
  while ( ! cin . eof () )
    if ( ! Command ( sheet, it, line, cmd, str1, str2, str3 ) )
      cout << "Wrong command" << endl;
}

bool Command ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 )
{
  cmd . clear ();
  str1 . clear ();
  str2 . clear ();
  str3 . clear ();
  getline ( cin, line );
  if ( line . empty () )                                                                                                            // when line is empty, nothing happens
    return true;
  it = line . begin ();
  SkipWS ( it );
  while ( isalpha ( ( int ) CurChar ( it ) ) )                                                                                      // gets command
    cmd += ( char ) toupper ( ( int ) GetChar ( it ) );
  if ( it == line . end () && cmd == "HELP" )                                                                                       // command is Help
  {
    Help ();
    return true;
  }
  if ( AnotherCommand ( sheet, it, line, cmd, str1, str2, str3 ) )                                                                  // command isn't Help
    return true;
  return false;
}

bool AnotherCommand ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 )
{
  if ( GetChar ( it ) == ' ' )
  {
    if ( cmd == "SAVE" || cmd == "LOAD" )                                                                                           // command is Save or Load
    {
      Rest ( it, line, str1 );                                                                                                      // gets file name
      if ( cmd == "SAVE" )                                                                                                          // command is Save
      {
        if ( ! sheet . Save ( str1 ) )
          cout << "Can't open the file" << endl;
      }
      else                                                                                                                          // command is Load
      {
        try
        {
          if ( ! sheet . Load ( str1 ) )
            cout << "Can't open the file" << endl;
        }
        catch ( InvalidExpressionException & e )
        {
          cout << "Wrong file format" << endl;
        }
        catch ( CycleException & e )
        {
          cout << "Wrong file format" << endl;
        }
      }
      return true;
    }
    if ( FillPrintErase ( sheet, it, line, cmd, str1, str2, str3 ) )                                                                // command isn't either Save or Load
      return true;
  }
  return false;
}

bool FillPrintErase ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 )
{
  if ( CellName ( it, str1 ) )                                                                                                      // gets cell name
  {
    if ( it == line . end () )
    {
      if ( cmd == "ERASE" )                                                                                                         // command is Erase
      {
        sheet . EraseCell ( str1 );
        return true;
      }
      if ( cmd == "PRINT" )                                                                                                         // command is Print
      {
        sheet . PrintCell ( str1, cout );
        return true;
      }
    }
    if ( GetChar ( it ) == ' ' )                                                                                                    // command isn't either Erase or Print
    {
      if ( cmd == "FILL" )                                                                                                          // command is Fill
      {
        Rest ( it, line, str2 );                                                                                                    // gets cell content
        try
        {
          sheet . FillCell ( str1, str2 );
        }
        catch ( InvalidExpressionException & e )
        {
          cout << "Invalid expression, try again" << endl;
        }
        catch ( CycleException & e )
        {
          cout << "Cycle formed, try again" << endl;
        }
        return true;
      }
      if ( Range ( sheet, it, line, cmd, str1, str2, str3 ) )                                                                       // command isn't either Fill
        return true;
    }
  }
  return false;
}

bool Range ( CSheet & sheet, string::const_iterator & it, string & line, string & cmd, string & str1, string & str2, string & str3 )
{
  if ( CellName ( it, str2 ) )                                                                                                      // gets second cell name
  {
    if ( it == line . end () )
    {
      if ( cmd == "ERASERANGE" )                                                                                                    // command is EraseRange
      {
        sheet . EraseRange ( str1, str2 );
        return true;
      }
      if ( cmd == "PRINTRANGE" )                                                                                                    // command is PrintRange
      {
        sheet . PrintRange ( str1, str2, cout );
        return true;
      }
    }
    if ( cmd == "FILLRANGE" && GetChar ( it ) == ' ' )                                                                              // command is FillRange
    {
      Rest ( it, line, str3 );                                                                                                      // gets cell content
      try
      {
        sheet . FillRange ( str1, str2, str3 );
      }
      catch ( InvalidExpressionException & e )
      {
        cout << "Invalid expression, try again" << endl;
      }
      catch ( CycleException & e )
      {
        cout << "Cycle formed, try again" << endl;
        sheet . EraseRange ( str1, str2 );                                                                                          // erases already filled cells
      }
      return true;
    }
  }
  return false;
}

void Help ( void )
{
  cout << "\nCommands:\nto fill or refill cell:  fill cellname expression\nto erase cell:           erase cellname\nto print cell:           print cellname\n"
       << "to fill range of cells:  fillrange firstcellname secondcellname expression\nto erase range of cells: eraserange firstcellname secondcellname\n"
       << "to print range of cells: printrange firstcellname secondcellname\nto save file:            save filename\nto load file:            load filename\n"
       << "to get help:             help\n\nIn expressions, you can use +, -, *, /, (), cell references,\nfunctions sin (), cos (), tg (), cotg ()\n"
       << "and aggregation functions sum (), avg (), min (), max ()\n(for example avg (a1:b3).\n\nTo fill cell with numeric expression, use = as the first character,\n"
       << "to fill cell with string with = as the first character, use \\ in front of =.\n\n";
}

int main ( void )
{
  Interface ();
  return 0;
}