#include "csheet.hpp"

CSheet::~CSheet ( void )
{
  for ( map < string, CCell * >::iterator it = m_cells . begin (); it != m_cells . end (); ++it )                                   // deletes every CCell
    delete it -> second;
}

void CSheet::FillCell ( const string & name, const string & content )
{
  string::const_iterator it = content . begin ();
  if ( CurChar ( it ) == '=' && content . length () > 1 )                                                                           // content is expression
  {
    Exp ( name, content );                                                                                                          // inserts expression type cell in spreadsheet
    return;
  }
  if ( m_cells . find ( name ) != m_cells . end () )                                                                                // erases cell if cell is already filled
    EraseCell ( name );
  SkipWS ( it );
  int sign = 1;                                                                                                                     // initial sign of possible number
  if ( CurChar ( it ) == '-' )                                                                                                      // unary '-' in front of possible number
  {
    sign = -1;                                                                                                                      // reverses number sign
    GetChar ( it );                                                                                                                 // gets '-'
  }
  SkipWS ( it );
  if ( isdigit ( ( int ) CurChar ( it ) ) )
  {
    double result = sign * Number ( it );
    SkipWS ( it );
    if ( it == content . end () )                                                                                                   // content is number
    {
      m_cells . insert ( make_pair ( name, new CNum ( result ) ) );                                                                 // inserts number type cell in spreadsheet
      return;
    }
  }
  m_cells . insert ( make_pair ( name, new CStr ( content ) ) );                                                                    // content is string, inserts string type cell in spreadsheet
}

void CSheet::EraseCell ( const string & name )
{
  try                                                                                                                               // deletes CCell
  {
    delete Search ( m_cells, name );
  }
  catch ( UnknownCellException & e )                                                                                                // if cell is empty, nothing happens
  {
    return;
  }
  m_cells . erase ( name );                                                                                                         // erases cell from map
}

void CSheet::PrintCell ( const string & name, ostream & os ) const
{
  try                                                                                                                               // prints cell, if cell is empty, prints newline
  {
    Search ( m_cells, name ) -> Print ( m_cells, name, os );
  }
  catch ( UnknownCellException & e )
  {
    os << endl;
  }
}

void CSheet::FillRange ( const string & first, const string & second, const string & content )
{
  string minCol;
  string maxCol;
  string minLine;
  string maxLine;
  RangeBoundaries ( first, second, minCol, maxCol, minLine, maxLine );                                                              // from corners it gets the most left and the most right column and top and bottom line
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
      FillCell ( col + line, content );                                                                                             // fills or refills cell with content
}

void CSheet::EraseRange ( const string & first, const string & second )
{
  string minCol;
  string maxCol;
  string minLine;
  string maxLine;
  RangeBoundaries ( first, second, minCol, maxCol, minLine, maxLine );                                                              // from corners it gets the most left and the most right column and top and bottom line
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
      EraseCell ( col + line );                                                                                                     // erases cell
}

void CSheet::PrintRange ( const string & first, const string & second, ostream & os ) const
{
  string minCol;
  string maxCol;
  string minLine;
  string maxLine;
  RangeBoundaries ( first, second, minCol, maxCol, minLine, maxLine );                                                              // from corners it gets the most left and the most right column and top and bottom line
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every column of range
  {
    os << "Col " << col << endl;                                                                                                    // prints column name
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )                                                            // and cell on every line
      PrintCell ( col + line, os );
  }
}

bool CSheet::Save ( const string & dstFile ) const
{
  ofstream ofs ( dstFile );
  if ( ofs . fail () )
    return false;
  for ( map < string, CCell * >::const_iterator it = m_cells . begin (); it != m_cells . end (); ++it )                             // prints every cell name and cell content in destination file
    ofs << it -> first << endl << it -> second -> GetContent () << endl;
  return true;
}

bool CSheet::Load ( const string & srcFile )
{
  CSheet tmp;
  ifstream ifs ( srcFile );
  if ( ifs . fail () )
    return false;
  string name;
  string content;
  while ( true )
  {
    getline ( ifs, name );                                                                                                          // gets cell name line
    if ( ! ifs . eof () )                                                                                                           // cell name line isn't last
    {
      NameCheck ( name );                                                                                                           // checks cell name is correct
      getline ( ifs, content );                                                                                                     // gets content line
      if ( ifs . eof () )                                                                                                           // content line is last, wrong file format
        throw InvalidExpressionException ();
      tmp . FillCell ( name, content );
    }
    else if ( name . empty () )                                                                                                     // cell name line is last and empty, it is correct
      break;
    else                                                                                                                            // cell name line is last but not empty, wrong file format
      throw InvalidExpressionException ();
  }
  for ( map < string, CCell * >::iterator it = m_cells . begin (); it != m_cells . end (); ++it )                                   // deletes old CCells
    delete it -> second;
  m_cells = tmp . m_cells;                                                                                                          // copies cell names and pointers to new CCells
  tmp . m_cells . clear ();                                                                                                         // erases temporary map of cell names and pointers to CCells
  return true;
}

void CSheet::Exp ( const string & name, const string & content )
{
  bool invRefInd = false;
  bool zeroDivInd = false;
  CExp ( content ) . Evaluate ( m_cells, name, invRefInd, zeroDivInd );                                                             // tries if evaluation doesn't throw exceptions
  if ( m_cells . find ( name ) != m_cells . end () )                                                                                // erases cell if cell is already filled
    EraseCell ( name );
  m_cells . insert ( make_pair ( name, new CExp ( content ) ) );                                                                    // inserts expression type cell in spreadsheet
}

void CSheet::NameCheck ( const string & name ) const
{
  string::const_iterator it = name . begin ();
  if ( isupper ( ( int ) CurChar ( it ) ) )                                                                                         // checks cell name is correct, there isn't '0' in the beginning of digital part and nothing follows
  {
    while ( isupper ( ( int ) GetChar ( it ) ) )
    {
    }
    --it;
    if ( isdigit ( ( int ) CurChar ( it ) ) && CurChar ( it ) != '0' )
    {
      while ( isdigit ( ( int ) GetChar ( it ) ) )
      {
      }
      --it;
      if ( it == name . end () )
        return;
    }
  }
  throw InvalidExpressionException ();
}