#include "functions.hpp"

char CurChar ( string::const_iterator & it )
{
  return *it;
}

char GetChar ( string::const_iterator & it )
{
  char tmp = *it;
  ++it;
  return tmp;
}

void SkipWS ( string::const_iterator & it )
{
  while ( isspace ( ( int ) GetChar ( it ) ) )
  {
  }
  --it;
}

double Number ( string::const_iterator & it )
{
  double result = GetChar ( it ) - '0';                                                                                             // value of first digit
  double decResult = 0;
  double decWeight = 10;
  while ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                      // adds other digits values and multiplies them by their weight
    result = 10 * result + GetChar ( it ) - '0';
  if ( CurChar ( it ) == '.' )                                                                                                      // number has decimal part
  {
    GetChar ( it );                                                                                                                 // gets '.'
    while ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                    // adds other digits values and multiplies them by their decimal weight
    {
      decResult += ( GetChar ( it ) - '0' ) / decWeight;
      decWeight *= 10;
    }
  }
  return result + decResult;
}

bool CellName ( string::const_iterator & it, string & name )
{
  if ( isalpha ( ( int ) CurChar ( it ) ) )                                                                                         // gets cell name, checks it's correct and there isn't '0' in the beginning of digital part
  {
    while ( isalpha ( ( int ) CurChar ( it ) ) )
      name += ( char ) toupper ( ( int ) GetChar ( it ) );
    if ( isdigit ( ( int ) CurChar ( it ) ) && CurChar ( it ) != '0' )
    {
      while ( isdigit ( ( int ) CurChar ( it ) ) )
        name += GetChar ( it );
      return true;
    }
  }
  return false;
}

void Rest ( string::const_iterator & it, const string & line, string & str )
{
  while ( it != line . end () )
    str += GetChar ( it );
}

CCell * Search ( const map < string, CCell * > & cells, const string & name )
{
  map < string, CCell * >::const_iterator it = cells . find ( name );
  if ( it == cells . end () )
    throw UnknownCellException ();
  return it -> second;
}

void RangeBoundaries ( const string & first, const string & second, string & minCol, string & maxCol, string & minLine, string & maxLine )
{
  string firstCol;
  string firstLine;
  string::const_iterator it = first . begin ();
  while ( isalpha ( ( int ) CurChar ( it ) ) )                                                                                      // gets column from first corner
    firstCol += ( char ) toupper ( ( int ) GetChar ( it ) );
  while ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                      // gets line from first corner
    firstLine += GetChar ( it );
  string secondCol;
  string secondLine;
  it = second . begin ();
  while ( isalpha ( ( int ) CurChar ( it ) ) )                                                                                      // gets column from second corner
    secondCol += ( char ) toupper ( ( int ) GetChar ( it ) );
  while ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                      // gets line from second corner
    secondLine += GetChar ( it );
  if ( firstCol . length () < secondCol . length () )                                                                               // compares which column is more left (minCol) or right (maxCol)
  {
    minCol = firstCol;
    maxCol = secondCol;
  }
  else if ( firstCol . length () > secondCol . length () )
  {
    minCol = secondCol;
    maxCol = firstCol;
  }
  else
  {
    minCol = min ( firstCol, secondCol );
    maxCol = max ( firstCol, secondCol );
  }
  if ( firstLine . length () < secondLine . length () )                                                                             // compares which line is more on top (minLine) or on bottom (maxLine)
  {
    minLine = firstLine;
    maxLine = secondLine;
  }
  else if ( firstLine . length () > secondLine . length () )
  {
    minLine = secondLine;
    maxLine = firstLine;
  }
  else
  {
    minLine = min ( firstLine, secondLine );
    maxLine = max ( firstLine, secondLine );
  }
}

string & ColumnIterate ( string & col )
{
  string::reverse_iterator it = col . rbegin ();
  while ( it != col . rend () )
  {
    ( *it )++;                                                                                                                      // iterates letter
    if ( *it == '[' )                                                                                                               // if letter was 'Z', changes it to 'A' and iterates next letter (order is from end to beginning)
    {
      *it = 'A';
      ++it;
      continue;
    }
    break;
  }
  if ( it == col . rend () )                                                                                                        // if all letters were 'Z', appends one 'A' to others (ZZ -> AAA)
    col . append ( "A" );
  return col;
}

string & LineIterate ( string & line )
{
  string::reverse_iterator it = line . rbegin ();
  while ( it != line . rend () )
  {
    ( *it )++;                                                                                                                      // iterates digit
    if ( *it == ':' )                                                                                                               // if digit was '9', changes it to '0' and iterates next digit (order is from end to beginning)
    {
      *it = '0';
      ++it;
      continue;
    }
    break;
  }
  if ( it == line . rend () )                                                                                                        // if all digits were '9', pushes '1' to beginning (99 -> 100)
    line = "1" + line;
  return line;
}