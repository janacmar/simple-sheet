#include "cexp.hpp"

CExp::CExp ( const string & exp ): m_exp ( exp )
{
}

string CExp::GetContent ( void ) const
{
  return m_exp;
}

void CExp::Print ( const map < string, CCell * > & cells, const string & name, ostream & os ) const
{
  bool invRefInd = false;
  bool zeroDivInd = false;
  double result = Evaluate ( cells, name, invRefInd, zeroDivInd );
  if ( invRefInd )                                                                                                                  // reference to string type cell appears somewhere in expression
    os << "Invalid Reference";
  else if ( zeroDivInd )                                                                                                            // dividing by zero appears somewhere in expression
    os << "Dividing By Zero";
  else
    os << result;
  os << " (" << m_exp << ')' << endl;
}

double CExp::Evaluate ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const
{
  string::const_iterator it = m_exp . begin ();
  ++it;                                                                                                                             // gets '='
  double result =  Expression ( it, cells, invRefInd, zeroDivInd, name );
  if ( it == m_exp . end () )
    return result;
  throw InvalidExpressionException ();
}

double CExp::Expression ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const
{
  double result = FirstSummand ( it, cells, invRefInd, zeroDivInd, name );                                                          // first summand may contain first factor with unary '-' in front of it
  while ( CurChar ( it ) == '+' || CurChar ( it ) == '-' )                                                                          // adds or substracts other summands in expression
  {
    if ( GetChar ( it ) == '+' )
      result += NextSummand ( it, cells, invRefInd, zeroDivInd, name );
    else
      result -= NextSummand ( it, cells, invRefInd, zeroDivInd, name );
  }
  return result;
}

double CExp::FirstSummand ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const
{
  SkipWS ( it );
  double result = FirstFactor ( it, cells, invRefInd, zeroDivInd, name );                                                           // first factor may have unary '-' in front of it
  Summand ( it, cells, invRefInd, zeroDivInd, name, result );                                                                       // multiplies or divides first factor of first summand by other factors
  return result;
}

double CExp::NextSummand ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const
{
  SkipWS ( it );
  double result = Factor ( it, cells, invRefInd, zeroDivInd, name );                                                                // first factor of summand except the first summand
  Summand ( it, cells, invRefInd, zeroDivInd, name, result );                                                                       // multiplies or divides first factor of summand (except the first summand) by other factors
  return result;
}

void CExp::Summand ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, double & result ) const
{
  char c;
  SkipWS ( it );
  while ( CurChar ( it ) == '*' || CurChar ( it ) == '/' )                                                                          // multiplies or divides first factor by other factors
  {
    c = GetChar ( it );
    SkipWS ( it );
    if ( c == '*' )
      result *= Factor ( it, cells, invRefInd, zeroDivInd, name );
    else
    {
      double tmp = Factor ( it, cells, invRefInd, zeroDivInd, name );
      if ( tmp == 0 )                                                                                                               // dividing by zero
      {
        result = 0;                                                                                                                 // summand result is set on zero (result doesn't matter anymore)
        zeroDivInd = true;                                                                                                          // corresponding indicator is set
      }
      else
        result /= tmp;
    }
    SkipWS ( it );
  }
}

double CExp::FirstFactor ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const
{
  if ( CurChar ( it ) == '-' )                                                                                                      // unary '-' in front of factor, returns negative value of factor
  {
    GetChar ( it );                                                                                                                 // gets '-'
    SkipWS ( it );
    return - Factor ( it, cells, invRefInd, zeroDivInd, name );
  }
  return Factor ( it, cells, invRefInd, zeroDivInd, name );
}

double CExp::Factor ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const
{
  if ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                         // factor is number
    return Number ( it );
  if ( CurChar ( it ) == '(' )                                                                                                      // factor is another expression
  {
    GetChar ( it );                                                                                                                 // gets '('
    double result = Expression ( it, cells, invRefInd, zeroDivInd, name );
    if ( GetChar ( it ) == ')' )                                                                                                    // gets ')'
      return result;
    throw InvalidExpressionException ();
  }
  string str;                                                                                                                       // factor is cell reference or function
  while ( isalpha ( ( int ) CurChar ( it ) ) )                                                                                      // reads alphabetical part of string
    str += ( char ) toupper ( ( int ) GetChar ( it ) );
  if ( isdigit ( ( int ) CurChar ( it ) ) && CurChar ( it ) != '0' )                                                                // string has digital part -> cell reference, checks if there isn't zero in the beginning (A01)
    return Reference ( it, cells, invRefInd, zeroDivInd, name, str );
  SkipWS ( it );
  if ( GetChar ( it ) == '(' )                                                                                                      // string has no digital part -> function, gets '('
  {
    double result = Function ( it, cells, invRefInd, zeroDivInd, name, str );
    if ( GetChar ( it ) == ')' )                                                                                                    // gets ')'
      return result;
  }
  throw InvalidExpressionException ();
}

double CExp::Reference ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & str ) const
{
  while ( isdigit ( ( int ) CurChar ( it ) ) )                                                                                      // reads digital part of string
    str += GetChar ( it );
  if ( str == name )                                                                                                                // checks referenced cell isn't original cell and cycle isn't formed
    throw CycleException ();
  try                                                                                                                               // finds cell, evaluates it, if cell is empty, returns zero
  {
    return Search ( cells, str ) -> Evaluate ( cells, name, invRefInd, zeroDivInd );
  }
  catch ( UnknownCellException & e )
  {
    return 0;
  }
}

double CExp::Function ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & fnc ) const
{
  if ( fnc == "SIN" )                                                                                                               // function is sinus
    return sin ( Expression ( it, cells, invRefInd, zeroDivInd, name ) );
  if ( fnc == "COS" )                                                                                                               // function is cosinus
    return cos ( Expression ( it, cells, invRefInd, zeroDivInd, name ) );
  if ( fnc == "TG" )                                                                                                                // function is tangens
  {
    double result = Expression ( it, cells, invRefInd, zeroDivInd, name );
    if ( cos ( result ) == 0 )                                                                                                      // tangens isn't defined for expression result
    {
      zeroDivInd = true;                                                                                                            // corresponding indicator is set
      return 0;                                                                                                                     // function result is set on zero (result doesn't matter anymore)
    }
    return sin ( result ) / cos ( result );
  }
  if ( fnc == "COTG" )                                                                                                              // function is cotangens
  {
    double result = Expression ( it, cells, invRefInd, zeroDivInd, name );
    if ( sin ( result ) == 0 )                                                                                                      // cotangens isn't defined for expression result
    {
      zeroDivInd = true;                                                                                                            // corresponding indicator is set
      return 0;                                                                                                                     // function result is set on zero (result doesn't matter anymore)
    }
    return cos ( result ) / sin ( result );
  }
  string first;                                                                                                                     // function is of aggregation type
  string second;
  Corners ( it, first, second );                                                                                                    // gets corners of range of cells
  string minCol;
  string maxCol;
  string minLine;
  string maxLine;
  RangeBoundaries ( first, second, minCol, maxCol, minLine, maxLine );                                                              // from corners it gets the most left and the most right column and top and bottom line
  if ( fnc == "SUM" )                                                                                                               // function is sum
    return Sum ( cells, invRefInd, zeroDivInd, name, minCol, maxCol, minLine, maxLine );
  if ( fnc == "AVG" )                                                                                                               // function is average
    return Avg ( cells, invRefInd, zeroDivInd, name, minCol, maxCol, minLine, maxLine );
  if ( fnc == "MIN" )                                                                                                               // function is minimum element
    return Min ( cells, invRefInd, zeroDivInd, name, minCol, maxCol, minLine, maxLine );
  if ( fnc == "MAX" )                                                                                                               // function is maximum element
    return Max ( cells, invRefInd, zeroDivInd, name, minCol, maxCol, minLine, maxLine );
  throw InvalidExpressionException ();
}

void CExp::Corners ( string::const_iterator & it, string & first, string & second ) const
{
  SkipWS ( it );
  if ( CellName ( it, first ) )                                                                                                     // first cell name
  {
    SkipWS ( it );
    if ( GetChar ( it ) == ':' )                                                                                                    // gets ':'
    {
      SkipWS ( it );
      if ( CellName ( it, second ) )                                                                                                // second cell name
      {
        SkipWS ( it );
        return;
      }
    }
  }
  throw InvalidExpressionException ();
}

double CExp::Sum ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const
{
  double sum = 0;
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
    {
      if ( col + line == name )                                                                                                     // checks cell isn't original cell and cycle isn't formed
        throw CycleException ();
      try                                                                                                                           // adds value of cell to sum, if cell is empty, nothing happens
      {
        sum += Search ( cells, col + line ) -> Evaluate ( cells, name, invRefInd, zeroDivInd );
      }
      catch ( UnknownCellException )
      {
      }
    }
  return sum;
}

double CExp::Avg ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const
{
  double sum = 0;
  int cnt = 0;                                                                                                                      // number of unempty cells
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
    {
      if ( col + line == name )                                                                                                     // checks cell isn't original cell and cycle isn't formed
        throw CycleException ();
      try                                                                                                                           // adds value of cell to sum and iterates number of cells, if cell is empty, nothing happens
      {
        sum += Search ( cells, col + line ) -> Evaluate ( cells, name, invRefInd, zeroDivInd );
        cnt++;
      }
      catch ( UnknownCellException )
      {
      }
    }
  if ( cnt == 0 )                                                                                                                   // range is empty, returns zero
    return 0;
  return sum / cnt;
}

double CExp::Min ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const
{
  set < double > values;
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
    {
      if ( col + line == name )                                                                                                     // checks cell isn't original cell and cycle isn't formed
        throw CycleException ();
      try                                                                                                                           // inserts cell value in sorted set, if cell is empty, nothing happens
      {
        values . insert ( Search ( cells, col + line ) -> Evaluate ( cells, name, invRefInd, zeroDivInd ) );
      }
      catch ( UnknownCellException )
      {
      }
    }
  if ( values . empty () )                                                                                                          // range is empty, returns zero
    return 0;
  return * values . begin ();                                                                                                       // returns first and therefore the minimum cell value
}

double CExp::Max ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const
{
  set < double > values;
  string colEnd = ColumnIterate ( maxCol );                                                                                         // column past the most right column of range of cells
  string lineEnd = LineIterate ( maxLine );                                                                                         // line past the bottom line of range of cells
  for ( string col = minCol; col != colEnd; ColumnIterate ( col ) )                                                                 // for every cell of range
    for ( string line = minLine; line != lineEnd; LineIterate ( line ) )
    {
      if ( col + line == name )                                                                                                     // checks cell isn't original cell and cycle isn't formed
        throw CycleException ();
      try                                                                                                                           // inserts cell value in sorted set, if cell is empty, nothing happens
      {
        values . insert ( Search ( cells, col + line ) -> Evaluate ( cells, name, invRefInd, zeroDivInd ) );
      }
      catch ( UnknownCellException )
      {
      }
    }
  if ( values . empty () )                                                                                                          // range is empty, returns zero
    return 0;
  return * values . rbegin ();                                                                                                      // returns last and therefore the maximum cell value
}