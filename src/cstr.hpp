#include "functions.hpp"

/** string type cell */
class CStr: public CCell
{
  public:
    /**
     * constructor, sets m_str on str
     *
     * @param[in] str string, cell content
     */
    CStr ( const string & str );

    /**
     * reads string the cell is filled by
     *
     * @return content of the cell
     */
    virtual string GetContent ( void ) const;

    /**
     * prints string (altered by escape sequence) in output stream
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the cell
     * @param[in,out] os output stream
     */
    virtual void   Print      ( const map < string, CCell * > & cells, const string & name, ostream & os ) const;

    /**
     * detects error
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the original cell
     * @param[in,out] invRefInd indicator of reference to string type cell, is set on true
     * @param[in,out] zeroDivInd indicator of dividing by zero
     * @return zero (result doesn't matter anymore)
     */
    virtual double Evaluate   ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const;                   // throws exception
  private:
    /** string, cell content */
    string m_str;
};