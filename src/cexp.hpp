#include "functions.hpp"

/** expression type cell */
class CExp: public CCell
{
  public:
    /**
     * constructor, sets m_exp on exp
     *
     * @param[in] exp expression, cell content
     */
    CExp ( const string & exp );

    /**
     * reads expression the cell is filled by
     *
     * @return content of the cell
     */
    virtual string GetContent ( void ) const;

    /**
     * prints numeric value of expression (if can't be evaluated, prints the reason) and original expression in brackets in output stream
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the cell
     * @param[in,out] os output stream
     */
    virtual void   Print      ( const map < string, CCell * > & cells, const string & name, ostream & os ) const;

    /**
     * evaluates numeric value
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the original cell, if this name appears in evaluated expression, cycle is detected
     * @param[in,out] invRefInd indicator of reference to string type cell in expression
     * @param[in,out] zeroDivInd indicator of dividing by zero in expression
     * @return numeric value of cell
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in expression
     */
    virtual double Evaluate   ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const;
  private:
    /** expression, cell content */
    string m_exp;

    /**
     * evaluates numeric value of expression (part of expression in brackets is also considered expression)
     *
     * @param[in,out] it iterator of expression to evaluate, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in expression
     * @param[in,out] zeroDivInd indicator of dividing by zero in expression
     * @param[in] name name of the original cell, if this name appears in evaluated expression, cycle is detected
     * @return numeric value of expression
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in expression
     */
    double Expression   ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const;

    /**
     * evaluates numeric value of first summand of expression
     *
     * @param[in,out] it iterator of first summand to evaluate, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in summand
     * @param[in,out] zeroDivInd indicator of dividing by zero in summand
     * @param[in] name name of the original cell, if this name appears in evaluated summand, cycle is detected
     * @return numeric value of summand
     * @throw InvalidExpressionException error in summand
     * @throw CycleException cycled references in summand
     */
    double FirstSummand ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const;

    /**
     * evaluates numeric value of summand (except the first one in expression)
     *
     * @param[in,out] it iterator of summand to evaluate, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in summand
     * @param[in,out] zeroDivInd indicator of dividing by zero in summand
     * @param[in] name name of the original cell, if this name appears in evaluated summand, cycle is detected
     * @return numeric value of summand
     * @throw InvalidExpressionException error in summand
     * @throw CycleException cycled references in summand
     */
    double NextSummand  ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const;

    /**
     * assistant method of FirstSummand and NextSummand, united part of their codes
     *
     * @param[in,out] it iterator of summand to evaluate, iterates from position past the first factor to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in rest of the summand
     * @param[in,out] zeroDivInd indicator of dividing by zero in rest of the summand
     * @param[in] name name of the original cell, if this name appears in rest of the summand, cycle is detected
     * @param[in,out] result first factor of summand sequentially multiplied/divided by other factors of summand 
     * @throw InvalidExpressionException error in rest of the summand
     * @throw CycleException cycled references in rest of the summand
     */
    void   Summand      ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, double & result ) const;

    /**
     * evaluates numeric value of first factor of summand
     *
     * @param[in,out] it iterator of first factor to evaluate, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in factor
     * @param[in,out] zeroDivInd indicator of dividing by zero in factor
     * @param[in] name name of the original cell, if this name appears in evaluated factor, cycle is detected
     * @return numeric value of factor
     * @throw InvalidExpressionException error in factor
     * @throw CycleException cycled references in factor
     */
    double FirstFactor  ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const;

    /**
     * evaluates numeric value of factor (except the first one in first summand)
     *
     * @param[in,out] it iterator of factor to evaluate, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in factor
     * @param[in,out] zeroDivInd indicator of dividing by zero in factor
     * @param[in] name name of the original cell, if this name appears in evaluated factor, cycle is detected
     * @return numeric value of factor
     * @throw InvalidExpressionException error in factor
     * @throw CycleException cycled references in factor
     */
    double Factor       ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name ) const;

    /**
     * evaluates numeric value of cell reference, if cell is empty, returns zero
     *
     * @param[in,out] it iterator of cell reference to evaluate, iterates from beginning of digital part of reference to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator that referenced cell is string or expression referencing string
     * @param[in,out] zeroDivInd indicator of dividing by zero in referenced cell
     * @param[in] name name of the original cell, if this name is referenced cell's name or appears in its expression, cycle is detected
     * @param[in] str alphabetical part of reference
     * @return numeric value of referenced cell
     * @throw CycleException cycled references detected
     */
    double Reference    ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & str ) const;

    /**
     * evaluates numeric value of expression and uses given function on result or evaluates aggregation function
     *
     * @param[in,out] it iterator of expression to evaluate or corners of range aggregation function is used on, iterates from beginning to end
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in expression or in range of cells aggregation function is used on
     * @param[in,out] zeroDivInd indicator of dividing by zero in expression or in range of cells aggregation function is used on
     * @param[in] name name of the original cell, if this name appears in evaluated expression or in range of cells aggregation function is used on, cycle is detected
     * @param[in] fnc name of function
     * @return result of function used on expression or aggregation function
     * @throw InvalidExpressionException error in expression, corners of range aggregation function is used on or function name
     * @throw CycleException cycled references in expression or in range of cells aggregation function is used on
     */
    double Function     ( string::const_iterator & it, const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & fnc ) const;

    /**
     * gets corners of range of cells aggregation function is used on
     *
     * @param[in,out] it iterator of corners to get, iterates from beginning to end
     * @param[out] first first corner, a cell name
     * @param[out] second second corner, a cell name
     * @throw InvalidExpressionException error in corners of range aggregation function is used on
     */
    void   Corners      ( string::const_iterator & it, string & first, string & second ) const;

    /**
     * evaluates summation of range of cells, if range is empty, returns zero
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in range of cells
     * @param[in,out] zeroDivInd indicator of dividing by zero in range of cells
     * @param[in] name name of the original cell, if this name appears in range of cells, cycle is detected
     * @param[in] minCol column in the most left position of range of cells
     * @param[in] maxCol column in the most right position of range of cells
     * @param[in] minLine top line of range of cells
     * @param[in] maxLine bottom line of range of cells
     * @return result of summation
     * @throw CycleException cycled references in range of cells
     */
    double Sum ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const;

    /**
     * evaluates average of range of cells, if range is empty, returns zero
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in range of cells
     * @param[in,out] zeroDivInd indicator of dividing by zero in range of cells
     * @param[in] name name of the original cell, if this name appears in range of cells, cycle is detected
     * @param[in] minCol column in the most left position of range of cells
     * @param[in] maxCol column in the most right position of range of cells
     * @param[in] minLine top line of range of cells
     * @param[in] maxLine bottom line of range of cells
     * @return average value
     * @throw CycleException cycled references in range of cells
     */
    double Avg ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const;

    /**
     * finds minimum value element of range of cells, if range is empty, returns zero
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in range of cells
     * @param[in,out] zeroDivInd indicator of dividing by zero in range of cells
     * @param[in] name name of the original cell, if this name appears in range of cells, cycle is detected
     * @param[in] minCol column in the most left position of range of cells
     * @param[in] maxCol column in the most right position of range of cells
     * @param[in] minLine top line of range of cells
     * @param[in] maxLine bottom line of range of cells
     * @return minimum value element
     * @throw CycleException cycled references in range of cells
     */
    double Min ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const;

    /**
     * finds maximum value element of range of cells, if range is empty, returns zero
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in,out] invRefInd indicator of reference to string type cell in range of cells
     * @param[in,out] zeroDivInd indicator of dividing by zero in range of cells
     * @param[in] name name of the original cell, if this name appears in range of cells, cycle is detected
     * @param[in] minCol column in the most left position of range of cells
     * @param[in] maxCol column in the most right position of range of cells
     * @param[in] minLine top line of range of cells
     * @param[in] maxLine bottom line of range of cells
     * @return maximum value element
     * @throw CycleException cycled references in range of cells
     */
    double Max ( const map < string, CCell * > & cells, bool & invRefInd, bool & zeroDivInd, const string & name, string & minCol, string & maxCol, string & minLine, string & maxLine ) const;
};