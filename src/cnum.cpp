#include "cnum.hpp"

CNum::CNum ( double num ): m_num ( num )
{
}

string CNum::GetContent ( void ) const
{
  stringstream ss;                                                                                                                  // double to string
  ss << m_num;
  return ss . str ();
}

void CNum::Print ( const map < string, CCell * > & cells, const string & name, ostream & os ) const
{
  os << m_num << endl;
}

double CNum::Evaluate ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const
{
  return m_num;
}