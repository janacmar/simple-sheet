#ifndef functions_hpp
#define functions_hpp

#include "ccell.hpp"

#include <iostream>
#include <sstream>
#include <set>
#include <cmath>
#include <fstream>

/** indicates error in expression */
class InvalidExpressionException
{
};

/** indicates cycled references in expression */
class CycleException
{
};

/** indicates cell is empty */
class UnknownCellException
{
};

char CurChar  ( string::const_iterator & it );                                                                                               // returns current character of iterated string
char GetChar  ( string::const_iterator & it );                                                                                               // gets next character of iterated string
void SkipWS   ( string::const_iterator & it );                                                                                               // skips whitespace of iterated string
double Number ( string::const_iterator & it );                                                                                               // gets numeric value of number from iterated string
bool CellName ( string::const_iterator & it, string & name );                                                                                // gets cell name from iterated string, indicates cell name is correct
void Rest     ( string::const_iterator & it, const string & line, string & str );                                                            // gets rest of iterated line
CCell * Search ( const map < string, CCell * > & cells, const string & name );                                                               // searches cell by its name
void RangeBoundaries ( const string & first, const string & second, string & minCol, string & maxCol, string & minLine, string & maxLine );  // from corners it gets the most left and the most right column and top and bottom line
string & ColumnIterate ( string & col );                                                                                                     // iterates column (from ZZ to AAA)
string & LineIterate   ( string & line );                                                                                                    // iterates line (from 9 to 10)

#endif