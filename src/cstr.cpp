#include "cstr.hpp"

CStr::CStr ( const string & str ): m_str ( str )
{
}

string CStr::GetContent ( void ) const
{
  return m_str;
}

void CStr::Print ( const map < string, CCell * > & cells, const string & name, ostream & os ) const
{
  string::const_iterator it = m_str . begin ();
  if ( CurChar ( it ) == '\\' )
  {
    while ( GetChar ( it ) == '\\' )
    {
    }
    --it;
    if ( CurChar ( it ) == '=' )                                                                                                    // escape sequence, string begins with a number of '\' characters and one '=' character
    {
      string str = m_str;
      str . erase ( 0, 1 );
      os << str << endl;                                                                                                            // prints string without the first '\' character
      return;
    }
  }
  os << m_str << endl;
}

double CStr::Evaluate ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const
{
  invRefInd = true;
  return 0;
}