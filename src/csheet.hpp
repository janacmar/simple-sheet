#include "cnum.hpp"
#include "cstr.hpp"
#include "cexp.hpp"

/** spreadsheet, container of cells */
class CSheet
{
  public:
    ~CSheet ( void );

    /**
     * fills or refills cell with content
     *
     * @param[in] name cell name
     * @param[in] content cell content
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in expression
     */
    void FillCell   ( const string & name, const string & content );

    /**
     * erases cell, empties content
     *
     * @param[in] name cell name
     */
    void EraseCell  ( const string & name );

    /**
     * prints cell in output stream, if cell is empty, prints newline
     *
     * @param[in] name cell name
     * @param[in,out] os output stream
     */
    void PrintCell  ( const string & name, ostream & os ) const;

    /**
     * fills or refills range of cells with content
     *
     * @param[in] first first corner of range of cells
     * @param[in] second second corner of range of cells
     * @param[in] content content of cells
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in an expression
     */
    void FillRange  ( const string & first, const string & second, const string & content );

    /**
     * erases range of cells, empties content
     *
     * @param[in] first first corner of range of cells
     * @param[in] second second corner of range of cells
     */
    void EraseRange ( const string & first, const string & second );

    /**
     * prints range of cells in output stream, if a cell is empty, prints newline
     *
     * @param[in] first first corner of range of cells
     * @param[in] second second corner of range of cells
     * @param[in,out] os output stream
     */
    void PrintRange ( const string & first, const string & second, ostream & os ) const;

    /**
     * saves spreadsheet data to file
     *
     * @param[in] dstFile new or existing file where data should be saved
     * @return indicator file was opened
     */
    bool Save ( const string & dstFile ) const;

    /**
     * loads data from file to spreadsheet
     *
     * @param[in] srcFile file data should be loaded from
     * @return indicator file was opened
     * @throw InvalidExpressionException wrong file format
     * @throw CycleException wrong file format
     */
    bool Load ( const string & srcFile );
  private:
    /** filled cells in spreadsheet */
    map < string, CCell * > m_cells;

    /**
     * assistant function for FillCell, fills cell with expression
     *
     * @param[in] name cell name
     * @param[in] content expression, cell content
     * @throw InvalidExpressionException error in expression
     * @throw CycleException cycled references in expression
     */
    void Exp       ( const string & name, const string & content );

    /**
     * assistant function for Load, checks cell name in loaded file is correct
     *
     * @param[in] name cell name
     * @throw InvalidExpressionException error in cell name, wrong file format
     */
    void NameCheck ( const string & name ) const;
};