#include "functions.hpp"

/** number type cell */
class CNum: public CCell
{
  public:
    /**
     * constructor, sets m_num on num
     *
     * @param[in] num number, cell content
     */
    CNum ( double num );

    /**
     * reads number the cell is filled by
     *
     * @return content of the cell
     */
    virtual string GetContent ( void ) const;

    /**
     * prints numeric value of cell in output stream
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the cell
     * @param[in,out] os output stream
     */
    virtual void   Print      ( const map < string, CCell * > & cells, const string & name, ostream & os ) const;

    /**
     * returns numeric value
     *
     * @param[in] cells other cells in spreadsheet
     * @param[in] name name of the original cell
     * @param[in,out] invRefInd indicator of reference to string type cell
     * @param[in,out] zeroDivInd indicator of dividing by zero
     * @return numeric value of cell
     */
    virtual double Evaluate   ( const map < string, CCell * > & cells, const string & name, bool & invRefInd, bool & zeroDivInd ) const;                    // returns m_num
  private:
    /** number, cell content */
    double m_num;
};