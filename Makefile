PROGRAM=janacmar

CC=g++
CFLAGS=-std=c++11 -Wall -pedantic -Wextra -g -Wno-long-long -O0 -ggdb

all: compile doc

run: compile
	./$(PROGRAM)

compile: $(PROGRAM)

doc: src/main.cpp src/ccell.hpp src/cexp.hpp src/cnum.hpp src/cstr.hpp src/csheet.hpp
	doxygen Doxyfile

$(PROGRAM): objs/main.o objs/ccell.o objs/cexp.o objs/cnum.o objs/cstr.o objs/csheet.o objs/functions.o
	$(CC) $(CFLAGS) $^ -o $@

objs/main.o: src/main.cpp src/ccell.hpp src/cexp.hpp src/cnum.hpp src/cstr.hpp src/csheet.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/csheet.o: src/csheet.cpp src/ccell.hpp src/cexp.hpp src/cnum.hpp src/cstr.hpp src/csheet.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/cstr.o: src/cstr.cpp src/ccell.hpp src/cstr.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@
	
objs/cnum.o: src/cnum.cpp src/ccell.hpp src/cnum.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/cexp.o: src/cexp.cpp src/ccell.hpp src/cexp.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/ccell.o: src/ccell.cpp src/ccell.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@

objs/functions.o: src/functions.cpp src/ccell.hpp src/functions.hpp | objs
	$(CC) $(CFLAGS) -c $< -o $@
	
objs: 
	mkdir objs
	
clean:
	rm -rf $(PROGRAM) objs/ doc/ examples/saved 2>/dev/null
